import psycopg2

class BooksGateway(object):
    def execute_database_command(self, command):
        conn = psycopg2.connect("dbname=library user=postgres")
        cursor = conn.cursor()
        cursor.execute(command)
        conn.commit()

    def execute_and_fetch_database_command(self, command):
        conn = psycopg2.connect("dbname=library user=postgres")
        cursor = conn.cursor()
        cursor.execute(command)
        result = cursor.fetchall()
        conn.commit()
        return result

    def get_all_books(self):
        return self.execute_and_fetch_database_command("SELECT * FROM books;")

    def add_new_book(self, book):
        self.execute_database_command("INSERT INTO books(ISBN, title, author, borrow_info, place) VALUES('{}', '{}', '{}', '{}', '{}');"
                                      .format(book.ISBN, book.title, book.author, book.borrow_info, book.place))

    def find_book_by_ISBN(self, ISBN):
        return self.execute_and_fetch_database_command("SELECT * FROM books WHERE ISBN = '{}'".format(ISBN))

    def find_books_by_title(self, title):
        return self.execute_and_fetch_database_command("SELECT * FROM books WHERE title = '{}'".format(title))

    def find_books_by_author(self, author):
        return self.execute_and_fetch_database_command("SELECT * FROM books WHERE author = '{}'".format(author))

    def update_borrow_info(self, ISBN, borrow_info):
        print("UPDATE books SET borrow_info = '{}' WHERE ISBN = '{}'".format(ISBN, borrow_info))
        self.execute_database_command("UPDATE books SET borrow_info = '{}' WHERE ISBN = '{}'".format(borrow_info, ISBN))