import datetime


class Book(object):
    def __init__(self, ISBN=None, title=None, author=None, borrow_info=None, place=None):
        self.ISBN = ISBN
        self.title = title
        self.author = author
        self.borrow_info = borrow_info
        self.place = place

    def get_book(self):
        if self.ISBN is None or self.ISBN == "":
            return BookWithoutISBN(self)
        return self

    @staticmethod
    def parse_database_result(database_result):
        books = []
        for i in database_result:
            books.append(Book.create_book_from_database(i))
        return books

    @staticmethod
    def create_book_from_database(database_result):
        ISBN = database_result[0]
        title = database_result[1]
        author = database_result[2]
        borrow_info = database_result[3]
        place = database_result[4]
        book = Book(ISBN, title, author, borrow_info, place)
        return book


class BookWithoutISBN(Book):
    def __init__(self, book):
        super().__init__(book.ISBN, book.title, book.author, book.borrow_info, book.place)
        self.ISBN = datetime.datetime.now()
