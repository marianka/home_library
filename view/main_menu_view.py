class MainMenuView(object):
    def __init__(self):
        pass

    @staticmethod
    def show_options():
        print("Main menu")
        print("Aby dodac ksiazke wpisz ADD")
        print("Aby wyszukać książkę wpisz FIND")
        print("Aby wypisać wszystkie książki wpisz ALL")
        print("Aby wypozyczyc ksiazke wpisz LEND")
        print("Aby zakonczysz wpisz CLOSE")
