class BookRepresentation(object):
    def __init__(self, books):
        self.__Books = books

    def __repr__(self):
        result = ""
        for book in self.__Books:
            result += "ISBN: {}\nTitle: {}\nAuthor: {}\nBorrow info: {}\nPlace: {}\n\n\n".format(book.ISBN,
                                                                                                 book.title,
                                                                                                 book.author,
                                                                                                 book.borrow_info,
                                                                                                 book.place)
        return result
