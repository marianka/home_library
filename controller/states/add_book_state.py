from controller.context import State
from model.books_gateway import BooksGateway
from model.Book import Book


class AddBookState(State):
    def __init__(self):
        pass

    def handle(self):
        title = input("Podaj nazwe\n")
        author = input("Podaj autora\n")
        ISBN = input("Podaj ISBN\n")
        borrow_info = input("Podaj informacje o wypozyczajacym\n")
        place = input("Podaj miejsce\n")

        book = Book(ISBN, title, author, borrow_info, place)
        books_gateway = BooksGateway()
        books_gateway.add_new_book(book.get_book())
        print(books_gateway.get_all_books())


