from controller.context import State
from controller.states.add_book_state import AddBookState
from controller.state_manager import StateManager
from controller.states.search_book_state import SearchBookState
from controller.states.print_all_books_state import PrintAllBooksState
from controller.states.lend_book_state import LendBookState
from view.main_menu_view import MainMenuView


class MainState(State):

    def __init__(self):
        self.state_manager = StateManager()

    def handle(self):
        while True:
            MainMenuView.show_options()

            command = input()
            if command == "ADD":
                new_state = AddBookState()
            elif command == "FIND":
                new_state = SearchBookState()
            elif command == "ALL":
                new_state = PrintAllBooksState()
            elif command == "LEND":
                new_state = LendBookState()
            else:
                break

            self.state_manager.setState(new_state)
            self.state_manager.request()


