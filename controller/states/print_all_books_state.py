from controller.context import State
from model.books_gateway import BooksGateway
from model.Book import Book
from view.Book_repr import BookRepresentation


class PrintAllBooksState(State):
    def __init__(self):
        pass

    def handle(self):
        books_gateway = BooksGateway()
        result = books_gateway.get_all_books()

        books = Book.parse_database_result(result)
        books_representation = BookRepresentation(books)
        print(books_representation)
