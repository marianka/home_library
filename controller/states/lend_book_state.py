from controller.context import State
from model.books_gateway import BooksGateway
from model.Book import Book
import datetime


class LendBookState(State):
    def __init__(self):
        pass

    def handle(self):
        ISBN, lender = self.get_info_from_user()

        borrow_info = "{}, {}".format(lender, str(datetime.datetime.now()))

        self.lend_book(ISBN, borrow_info)

    def get_info_from_user(self):
        ISBN = input("Podaj ISBN książki którą chcesz wypożyczyć\n")
        lender = input("Podaj informacje o wypozyczajacym(nick/imię)\n")
        return ISBN, lender

    def lend_book(self, ISBN, borrow_info):
        books_gateway = BooksGateway()
        if self.check_if_free(ISBN, books_gateway):  # check if free
            books_gateway.update_borrow_info(ISBN, borrow_info)
        # add borrow_info
        else:
            print("Wypozyczona")

    def check_if_free(self, ISBN, books_gateway):
        res = books_gateway.find_book_by_ISBN(ISBN)
        books = Book.parse_database_result(res)  # list
        book = books[0]
        if book.borrow_info is None or book.borrow_info == "":
            return True
        return False
