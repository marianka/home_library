from controller.context import State
from model.books_gateway import BooksGateway
from model.Book import Book
from view.Book_repr import BookRepresentation


class SearchBookState(State):
    def __init__(self):
        pass

    def handle(self):
        books_gateway = BooksGateway()
        search_by = input("""Szukaj według:
        1. ISBN
        2. Nazwy
        3. Autora
        Wybierz odpowiednią cyfre\n""")
        if search_by == "1":
            ISBN = input("Podaj ISBN do wyszukania\n")
            res = books_gateway.find_book_by_ISBN(ISBN)
        elif search_by == "2":
            title = input("Podaj tytuł do wyszukania\n")
            res = books_gateway.find_books_by_title(title)
        elif search_by == "3":
            author = input("Podaj autora do wyszukania\n")
            res = books_gateway.find_books_by_author(author)
        else:
            print("Podałeś błędną wartość")
            return

        books = Book.parse_database_result(res)
        books_representation = BookRepresentation(books)
        print(books_representation)

