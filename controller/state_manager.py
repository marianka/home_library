
class StateManager(object):
    def __init__(self, state=None):
        self.current_state = state

    def setState(self, state):
        self.current_state = state

    def request(self):
        self.current_state.handle()
