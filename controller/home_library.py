from controller.states.main_state import MainState
from controller.state_manager import StateManager

class HomeLibrary(object):

    def __init__(self):
        """Inicjalizacja stanow i odpalenie pierwszego z nich"""
        self.state_manager = StateManager()
        self.main_state = MainState()#inicjalizacja pierwszego stanu
        self.state_manager.setState(self.main_state)
        self.state_manager.request()


if __name__ == "__main__":
    home_library = HomeLibrary()


